import {Header} from './components/Header';
import {CurrentWeather} from './components/CurrentWeather';
import {Forecast} from './components/Forecast';

export const App = () => (
    <main>
        <Header/>
        <CurrentWeather/>
        <Forecast/>
    </main>
);
