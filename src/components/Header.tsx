import {Filter} from './Filter';

export const Header = () => (
    <>
        <Filter />
        <div className="head">
            <div className="icon rainy"></div>
            <div className="current-date">
                <p>вторник</p>
                <span>6 апреля</span>
            </div>
        </div>
    </>
);
