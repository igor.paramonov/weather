FROM node:15-alpine

WORKDIR /app

# enables a polling mechanism via chokidar (which wraps fs.watch, fs.watchFile, and fsevents) so that hot-reloading will work.
ENV CHOKIDAR_USEPOLLING=true

RUN yarn global add react-scripts

CMD ["/entrypoint.sh"]
